require stream
require keysight_dsox1204a,master
require iocstats

epicsEnvSet("TOP",	"$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME",	"TS2-010RFC:SC-IOC-009")
epicsEnvSet("P",	"TS2-010CRM:")
epicsEnvSet("R",	"EMR-SCOPE-020:")
epicsEnvSet("ASYN_PORT","DSOX1204a")
epicsEnvSet("CH1",      "1")
epicsEnvSet("CH2",      "2")
epicsEnvSet("CH3",      "3")
epicsEnvSet("CH4",      "4")
epicsEnvSet("IP",	"192.168.1.2")
# epicsEnvSet("IP",	"scope-dsox1204a-ts2.tn.esss.lu.se")
# epicsEnvSet("COMPANY", "European Spallation Source ERIC")
# epicsEnvSet("ENGINEER","MuYuan Wang <MuYuan.Wang@ess.eu>")

# Add extra startup scripts requirements here
iocshLoad("$(iocstats_DIR)/iocStats.iocsh")
iocshLoad("${keysight_dsox1204a_DIR}keysight_dsox1204a.iocsh", "P=$(P), R=$(R), IP=$(IP), CH1=$(CH1), CH2=$(CH2), CH3=$(CH3), CH4=$(CH4), ASYN_PORT=$(ASYN_PORT)")

# Call autosave function
# afterInit("fdbrestore("$(AS_TOP)/$(IOCNAME)/save/$(SETTINGS_FILES).sav")")

# Start any sequence programs
seq("sncProcess", "P=$(P), R=$(R)")

# Call iocInit to start the IOC
iocInit()
