#include <string.h>    // Provides memcpy prototype
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <registryFunction.h>

#define MAXPOINTS 5001
/*
 * xaxis/yaxis waveform calculation
 *
 * Parameters:
 *
 *  INA: measurement status
 *  INB: channel status
 *  INC: data points
 *  IND: xincrement
 *  INE: xorigin
 *  INF: yincrement
 *  ING: yorigin
 *  INH: yreference
 *  INI: raw data
 *
 *  VALA: @param [out] [waveform] xaxis
 *  VALB: @param [out] [waveform] yaxis
 *  VALC: @param [out] [scalar] yaxis maximum
 *
 */
static int waveform_calculation(aSubRecord *precord)
{
    const short mesWF   = *((short*)precord->a);
    const short channel = *((short*)precord->b);
    const long points   = *((long*)precord->c);
    if (points>MAXPOINTS) {
        printf("[aSub ERROR] data points is too high\n");
        return -1;
    }
    if (points>precord->nova) {
        printf("[aSub Error] data points is bigger than NOVA");
        return -1;
    }
    // parse parameters
    const double xinc = *((double*)precord->d);
    const double xor  = *((double*)precord->e);
    const double yinc = *((double*)precord->f);
    const double yor  = *((double*)precord->g);
    const long   yref = *((long*)precord->h);
    // parsing waveform
    if (mesWF==0 || (mesWF==1 && channel==0)) {
        memset(precord->vala, 0, precord->nova*sizeof(double));
        precord->neva=0;
        memset(precord->valb, 0, precord->novb*sizeof(double));
        precord->nevb=0;
    }
    else if (mesWF==1 && channel==1) {
        int i;
        double *input_wave = (double*)precord->i;
        double *xaxis, *yaxis;
        xaxis = (double *)malloc(points*sizeof(double));
        if (xaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        yaxis = (double *)malloc(points*sizeof(double));
        if (yaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        double max=yaxis[0];
        for (i=0; i<points; i++) {
            xaxis[i]=i*xinc+xor;
            yaxis[i]=(input_wave[i]-yref)*yinc+yor;
            if (yaxis[i]>=max) {max=yaxis[i];}
        }
        memcpy(precord->vala, xaxis, points*sizeof(double));
        precord->neva=points;
        memcpy(precord->valb, yaxis, points*sizeof(double));
        precord->nevb=points;
        *((double*)precord->valc) = max;
        precord->nevc=1;
        free(xaxis);
        free(yaxis);
    }
    /* Done! */
    return 0;
}
epicsRegisterFunction(waveform_calculation);
